# First modification

We added the first improvement proposed in the document.
So we store the mark for each number in a single bit, which reduce the memory footprint by 8 times.

# Sequential algorithm

Time is the mean of 5 measurement, done on a Intel(R) Core(TM) i7-4800MQ CPU @ 2.70GH (6 cores)

| Max Number | Time       |
| ---------- | ---------- |
| 500        | 0 ms       |
| 5 000      | 0.1 ms     |
| 5 000 000  | 40.7 ms    |
| 5e8        | 6668.9ms   |
| 5e9        | 7440.62 ms |

# Implement OMP

We added OMP parallelisation at one specific in [our code](./main_working.cpp).
line 80 for the global eratosthenes algorithm.

## Measurements

### Own computer

Time is the mean of 5 measurement, done on a Intel(R) Core(TM) i7-4800MQ CPU @ 2.70GH (6 cores)

| Max Number | Time      |
| ---------- | --------- |
| 500        | 1.78 ms   |
| 5 000      | 4.84 ms   |
| 5 000 000  | 28.2 ms   |
| 5e8        | 4563.72ms |
| 5e9        | XXXXX     |

My computer freeze when reaching 5e9, as you can see on this screen :
![40](./img/htop.jpg)
Even by changin the size of the chunk for each threads (2 to 1000), it didn't solved the problem.

# Dione

Fabien tried with his credential:
On dione when I'm typing the command

```
srun -N 2 -t 10:00 --mem=2G main_working 500
```

or

```
srun -n 2 -t 10:00 --mem=2G main_working 500
```

It execute the program twice and not with multiple cores
