#include <iostream>
#include <math.h>
#include <omp.h>
// #define USE_BITS

#define DEBUG 0 /* Set to 1 if you want a lot of output */
#define to_number(i) 2 * (i) + 1
#define to_array_index(i) (i) / 2

const unsigned char unmarked = 0x00;
const unsigned char marked = 0x01;

#ifdef USE_BITS

#define get_value(prime, index) ((prime[(index)] >> ((index) % 8)) & 0x01)
#define set(prime, index) (prime[(index)] |= 1U << ((index) % 8))
#define unset(prime, index) (prime[(index)] &= ~(1U << ((index) % 8)))

#else

#define get_value(prime, index) prime[index]
#define set(prime, index) prime[index] = marked
#define unset(prime, index) prime[index] = unmarked

#endif

#define set_marked(prime, index) set(prime, index)
#define set_unmarked(prime, index) unset(prime, index)

#define is_unmarked(prime, index) (get_value(prime, index) == unmarked)

#define copy_value(array1, array2, index)                                      \
  (set_value(array2, index, get_value(array1, index)))

int main(int argc, char const *argv[]) {
  /* code */
  long i, k, t, N, N_pos, nr_primes, tid, nthreads, cc = 0, lastnumber,
                                                    lastprime = 0;
  unsigned char *prime = NULL;
  // 1 byte =
  double start, stop;

  if (argc < 2) {
    printf("Usage:  %s N\n", argv[0]);
    exit(-1);
  }
#ifdef _OPENMP
  nthreads = omp_get_max_threads();
  printf("OpenMp is enabled\n");
  printf("%ld threads available\n\n", nthreads);
#endif
  N = atol(argv[1]);
  N_pos = (N - 1) / 2;

  start = omp_get_wtime(); /* Start measuring time */
/* Allocate marks for all odd integers from 3 to N */
// store 1 mark per bit instead of per byte
#ifdef USE_BITS
  long total_allocated = (N_pos / 8) + 1;
#else
  long total_allocated = N_pos + 1;
#endif
  lastnumber = (int)sqrt((double)N);
  prime = (unsigned char *)malloc(total_allocated);
  if (prime == NULL) {
    printf("Could not allocate %ld chars of memory\n", N_pos);
    exit(-1);
  } else {
    printf("allocated %ld bytes for a total of %ld positions\n",
           total_allocated, N_pos);
  }

  /* Mark primes[0] since that is not used */
  set_marked(prime, 0);
  /* Initialize all odd numbers to unmarked */
  // #pragma omp parallel for shared(prime)
  for (i = 0; i <= N_pos; i++)
    set_unmarked(prime, i);

#pragma omp parallel for shared(prime) private(i, k)                           \
    schedule(static, 2) if (N > 20)
  // for (Number i = 3; i <= lastNumberSqrt; i += 2)
  for (i = 3; i <= lastnumber; i += 2) {
    tid = omp_get_thread_num();
    if (is_unmarked(prime, to_array_index(i))) {
    // if (prime[i / 2] == unmarked) {
      if (DEBUG) {
        printf("Marking multiples of %ld [%ld|%ld]: ", i, i * i, N);
      }
      for (k = i * i; k <= N; k += 2 * i) {
        set_marked(prime, to_array_index(k));
        // prime[k / 2] = marked;
        if (DEBUG)
          printf("{%ld:%ld > %ld | %ld} ", i, k, tid, N);
      }
      if (DEBUG)
        printf("\n");
    }
  }

  if (DEBUG) {
// print bytes
#ifdef USE_BITS
    for (int bytes_count = 0; bytes_count < total_allocated; bytes_count++) {
      printf("b%d: ", bytes_count);
      for (int bit_index = 0; bit_index < 8; bit_index++) {
        printf("%d", get_value(prime, 8 * bytes_count + bit_index));
      }
      printf("\n");
    }
#endif
  }
  // delete[] isPrime;
  nr_primes = 1; /* Remember to count 2 as a prime */
  /* Count the marked numbers */
  for (i = 1; i <= N_pos; i++) {
    if (is_unmarked(prime, i)) {
      lastprime = to_number(i);
      nr_primes++;
    }
  }

  stop = omp_get_wtime();
  printf("Time: %6.1f ms\n", (float)(stop - start) * 1000);

  if (DEBUG) {
    printf("\nPrime numbers smaller than or equal to %ld are\n", N);
    printf("%d ", 2); /* Remember to print the value 2 */
    for (i = 1; i <= N_pos; i++) {
      if (is_unmarked(prime, i)) {
        // if (prime[i] == unmarked) {
        printf("%ld ", to_number(i));
      }
    }
  }

  printf("\n%ld primes smaller than or equal to %ld\n", nr_primes, N);
  printf("The largest of these primes is %ld\n", lastprime);
  printf("\nReady\n");
  exit(0);

  return 0;
}
