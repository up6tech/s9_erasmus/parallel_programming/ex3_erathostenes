#include <cstring>
#include <iostream>
#include <malloc.h>
#include <omp.h>
#include <stdio.h>
#include <stdlib.h>

const unsigned char unmarked = 0x00;
const unsigned char marked = 0x01;

#define get_value(prime, index) ((prime >> (index % 8)) & 0x01)
#define set(prime, index) (prime |= 1U << (index % 8))
#define unset(prime, index) (prime &= ~(1U << (index % 8)))

#define set_marked(prime, index) set(prime, index)
#define set_unmarked(prime, index) unset(prime, index)

#define is_unmarked(prime, index) (get_value(prime, index) == unmarked)

#define copy_value(array1, array2, index)                                      \
  (set_value(array2, index, get_value(array1, index)))

int main(int argc, char const *argv[]) {

  unsigned char b = 0;
  std::cout << "Data at index 0 " << get_value(b, 0) << "." << std::endl;
  // set byte to
  set_marked(b, 0);
  std::cout << "Data at index 0 " << get_value(b, 0) << "." << std::endl;
  set_unmarked(b, 0);
  std::cout << "Data at index 0 " << get_value(b, 0) << "." << std::endl;

  return 0;
}
